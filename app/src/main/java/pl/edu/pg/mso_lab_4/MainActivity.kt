package pl.edu.pg.mso_lab_4

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    var isBound = false
    var mMessenger: Messenger? = null

    val textBox: TextView by lazy { findViewById(R.id.serviceOutput) }

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            isBound = true
            mMessenger = Messenger(service)
            textBox.text = "Bound!"
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mMessenger = null
            isBound = false
            Log.v("servconn", "service disconnected");
        }
    }
    inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            if (msg.what == 321) {
                textBox.text = msg.data.getString("str1")
            }
        }
    }

    val replyMessenger: Messenger = Messenger(IncomingHandler())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun connect(view: View) {
        val messenger = Intent()
        messenger.setClassName("pl.edu.pg.mso_lab_4", "pl.edu.pg.mso_lab_4.MessengerService")
        bindService(messenger, serviceConnection, Context.BIND_AUTO_CREATE)
        textBox.text = "Binding..."
    }

    fun sendMessage(view: View) {
        if (isBound) {
            val msg: Message = Message.obtain(null, 123, 0, 0)
            msg.replyTo = replyMessenger
            val bundle = Bundle()
            bundle.putString("str1", (0..99999).random().toString() + " is my random value!")
            msg.data = bundle
            try {
                mMessenger?.send(msg)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }
    }

    override fun onBackPressed() {
        unbindService(serviceConnection)
        finish()
    }
}