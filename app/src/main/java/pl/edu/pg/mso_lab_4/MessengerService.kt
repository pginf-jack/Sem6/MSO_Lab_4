package pl.edu.pg.mso_lab_4

import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log


class MessengerService : Service() {
    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            if (msg.what == 123) {
                val msg_out: Message = Message.obtain(null, 321, 0, 0)
                val bundle = Bundle()
                bundle.putString(
                    "str1",
                    (0..99999).random().toString() + " received: " + msg.data.getString("str1")
                )
                msg_out.data = bundle
                try {
                    msg.replyTo.send(msg_out)
                } catch (e: RemoteException) {
                    e.printStackTrace()
                }
            }
        }
    }

    val mMessenger: Messenger = Messenger(IncomingHandler())

    override fun onBind(intent: Intent?): IBinder? {
        return mMessenger.binder
    }

    override fun onDestroy() {
        Log.i("service", "service destroyed!")
    }
}